package linkedList;

public class Creation {
	public static void  main(String [] args) {
		LinkedList list = new LinkedList();
		list.createNodes(10);
		list.dispLinkedList();
		list.deleteNode(3);
		list.dispLinkedList();
		list.deleteNode(10);
		list.dispLinkedList();
		list.deleteNode(1);
		list.dispLinkedList();
		
	}

}

class LinkedList{
	 Node head;
	class Node{
	 int value;
	 Node next;
	
	
	
	Node(int value, Node next){
		this.value = value;
		this.next = next;
	}
	}
	
	
	public void createNodes(int n) {
		Node prev = null;
		Node cNode = null;
		for(int i =n;i>0;i--) {
		  cNode = 	new Node(i,prev);
		  prev = cNode;
		}
		this.head = cNode;
	}
	
	 void dispLinkedList() {
		System.out.println("Linked List values are");
		Node temp = head;
		while(temp != null) {
			System.out.print(temp.value + " ");
			temp = temp.next;
		}
		System.out.println();
	}
	
	 void deleteNode( int valueToBeDeleted) {
		if(head.value == valueToBeDeleted) {
			head = head.next;
			return;
		}
		Node prev = null;
		Node temp = head;
		while(temp != null) {
			if(temp.value == valueToBeDeleted) {
				prev.next = temp.next;
			}
			prev = temp;
			temp = temp.next;
		}
	}
}