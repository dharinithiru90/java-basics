package conversion;


public class IntegertoDouble {
	
	public static void main(String[] args) {
	 int numerator = 15;
     double result = numerator / 5;

     // Print statement
     System.out.println("Result is in double format : "
                        + result);
	}

}


//print the acsii value of int

class printAsciiValue{
	public static void main(String[] args) {
		char a = 'A';
		int i = a;
		
		System.out.println("Ascii value " + i);
	}
}