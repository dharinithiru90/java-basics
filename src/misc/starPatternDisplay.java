package misc;

import java.util.Scanner;

public class starPatternDisplay {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		for (int i = n; i > 0; i--) {
			for (int j = 0; j <= i; j++) {
				System.out.print("*" + " ");
			}
			System.out.println();
		}
		scan.close();
	}
}

class pyramidNumberPattern {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int row = 1;
		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n + n - 1; j++) {

				System.out.print("*");
			}
			System.out.println();
		}
	}
}

class starPattern1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++)
				System.out.print("*");
			System.out.println();
		}
		scanner.close();
	}
}

class starPattern2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j <= n; j++)
				System.out.print("*");
			System.out.println();
		}
		scanner.close();
	}
}

class starPattern3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();

		}
		for (int i = 1; i < n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
		scanner.close();
	}
}

class starPattern4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			System.out.println();

		}
		scanner.close();
	}
}

class starPattern5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j < i; j++) {
				System.out.print(" ");
			}
			for (int k = i; k <= n; k++) {
				System.out.print("*");
			}
			System.out.println();

		}
		scanner.close();
	}
}

class starPattern6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}

		for (int i = 1; i < n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(" ");
			}
			for (int k = i; k < n; k++) {
				System.out.print("*");
			}
			System.out.println();
		}

		scanner.close();
	}
}

class starPattern7 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				if (k == 1)
					System.out.print("*");
				else {
					System.out.print(" *");
				}
			}
			System.out.println();

		}
		scanner.close();
	}
}

class starPattern8 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			for (int k = 1; k < i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}

		scanner.close();

	}
}

class starPattern9 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j < i; j++) {
				System.out.print(" ");
			}
			System.out.print("*");
			System.out.println();
		}
		scanner.close();
	}
}

class starPattern10 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print(" ");
			}
			System.out.print("*");
			System.out.println();
		}
		scanner.close();
	}
}

class starPattern11 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j < n; j++) {
				System.out.print(" ");
			}
			for (int j = 1; j <= i; j++) {
				if (j == 1)
					System.out.print("*");
				else
					System.out.print(" ");
			}
			for (int k = 2; k < i; k++)
				System.out.print(" ");
			if (i > 1)
				System.out.print("*");

			System.out.println();
		}
		scanner.close();
	}
}

class starPattern12 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j < i; j++) {
				System.out.print(" ");
			}
			for (int k = i; k <= n; k++) {
				if (i == k)
					System.out.print("*");
				else
					System.out.print(" ");
			}

			for (int k = i; k < n; k++) {
				if (n - 1 == k)
					System.out.print("*");
				else
					System.out.print(" ");
			}

			System.out.println();
		}
		scanner.close();
	}
}

class starPattern13{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int k = scanner.nextInt();
		for(int i = 1;i<=n;i++) {
			for(int j=1;j<=k;j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		scanner.close();
}
}

class starPattern14{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int k = scanner.nextInt();
		for(int i = 1;i<=n;i++) {
			for(int j=1;j<=k;j++) {
				if(i==j || i+j== n+1)
				System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
		
		scanner.close();
}
}


class starPattern15{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int k = scanner.nextInt();
		for(int i = 1;i<=n;i++) {
			for(int j=1;j<=k;j++) {
				if(i==1 || j==1|| i==n|| j==k )
				System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
		
		scanner.close();
}
}

class starPattern16{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		
		for(int i = 1;i<=n;i++) {
			for(int j=1;j<i;j++)
				System.out.print(" ");
			for(int k=i;k<=7;k++)
				System.out.print("* ");
			System.out.println();
		}
		for(int i = 1;i<=n;i++) {
			for(int j=i;j<n;j++) {
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++)
				System.out.print("* ");
			System.out.println();
		}
		scanner.close();
}
}

