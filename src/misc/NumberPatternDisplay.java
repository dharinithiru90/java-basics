package misc;

import java.util.Scanner;

public class NumberPatternDisplay {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(i + " ");
			}
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int cn = 1;
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(cn + " ");
				cn++;
			}
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = i; j >= 1; j--) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			for (int k = i - 1; k >= 1; k--) {
				System.out.print(k + " ");
			}
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = n; j >= i; j--)
				System.out.print(j + " ");
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int cnt = 1;
		for (int i = 1; i <= n; i++) {
			cnt = i;
			for (int j = 1; j <= i; j++) {
				System.out.print(cnt + " ");
				cnt = cnt + n - j;
			}
			System.out.println();
		}
		scanner.close();
	}
}

class NumberPattern7 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		int cnt = 1;
		for (int i = 1; i <= a; i++) {
			if (i % 2 == 0) {
				int temp = cnt+b;
				for (int j = 1; j <= b; j++) {
					System.out.print(temp-j + " ");
                    
					cnt++;
				}
			} else {
				for (int j = 1; j <= b; j++) {
					System.out.print(cnt + " ");
					cnt++;
				}
			}
			System.out.println();
		}
		scanner.close();
	}
}

class pascalTrianglePattern{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for(int i=0;i<=n;i++) {
			for(int k=i;k<=n;k++)
				System.out.print(" ");
			for(int j=0;j<=i;j++) {
				System.out.print(factorial(i)/(factorial(j)* factorial(i-j)) + " ");
			}
			System.out.println();
		}
		scanner.close();
	}
	
	static int factorial(int n) {
		if(n>0) {
		if(n==1)
			return 1;
		else {
			return n*factorial(n-1);
		}
		}else {
			return 1;
		}
	}
}

