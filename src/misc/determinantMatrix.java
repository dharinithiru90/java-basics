package misc;

import java.util.Scanner;

public class determinantMatrix {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int arr[][] = new int[n][n];
		readMultiDArrayInput(arr,n,scanner);
		dispMultiDArray(arr,n);
		if(n == 1) {
			System.out.println(arr[0][0]);
		}else if(n == 2) {
			int delta = determinant2cross2(arr);
			System.out.println(delta);
		}
		else if(n == 3) {
			int delta  = 0;
			int tempIIndex = 0;
			int  tempJIndex = 0;
			int a[][] = new int[n][n];
			int i =0;

    int j =0;
		//  while(j<n) {
				
				for(int k=0;k<n;k++) {
					for(int l=0;l<n;l++) {
						if(i!=k && j!=l) {
							
							
							a[tempIIndex][tempJIndex] = arr[k][l];
							tempJIndex++;
							
							if(tempJIndex ==  n-1) {
								 tempIIndex++;
								tempJIndex=0;
						   }
						}
						
					}
					
					
				}
			//	dispMultiDArray(a,2);
				delta+= a[i][j]*determinant2cross2(a);
				j++;
			//}
		     System.out.println(delta);
		}
	}
   
	
	
	private static int determinant2cross2(int[][] a) {
		return a[0][0]*a[1][1] - a[1][0]*a[0][1];
		
	}

	static void readMultiDArrayInput(int[][] arr, int n, Scanner scanner) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				arr[i][j] = scanner.nextInt();
			}
		}
	}
	
	static void dispMultiDArray(int[][] arr, int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(arr[i][j]+ " ");
			}
			System.out.println();
		}
	}
}
