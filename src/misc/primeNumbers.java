package misc;

import java.util.Scanner;

public class primeNumbers {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		printPrime(n);
		scanner.close();
	}

	private static void printPrime(int n) {
		for(int i =2;i<n;i++) {
			boolean isPrime = true;
			for(int j = 2;j<=Math.sqrt(i);j++) {
				if( i%j ==0) {
					isPrime = false;
					break;
				}
				
			}
			if(isPrime)
				 System.out.println(i);
			
		}
		
	}

}
