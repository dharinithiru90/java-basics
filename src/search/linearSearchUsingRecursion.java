package search;

import java.util.Scanner;

public class linearSearchUsingRecursion {

	public static void main(String[] args) {
		int[] arr = { 10, 20, 30, 45, 60 };
		Scanner scanner = new Scanner(System.in);
		int x = scanner.nextInt();
		int index = linearSearch(arr, x, 0, arr.length);
		System.out.println("Index "+index);
		scanner.close();

	}

	private static int linearSearch(int[] arr, int x, int i, int length) {
		if (i < length) {
			if (arr[i] == x)
				return i;
			else {
				return linearSearch(arr, x, i + 1, arr.length);
			}
		}
		return -1;
	}

}
